import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class main2 {
    public static void main(String[] args) {
        int startNum = 723;
        String[] operations = {"*2", "/2", "+545", "/3","*5" };
        List<String> allPermutations = generatePermutations(Arrays.asList(operations));
        System.out.println("allPermutations = " + Arrays.asList(allPermutations));
        System.out.println("allPermutations.size = " + allPermutations.size());
        double maxValue = Double.NEGATIVE_INFINITY;
        String bestSequence = null;

        for (String permutation : allPermutations) {
            double result = calculate(startNum, permutation.split(","));
            if (result > maxValue ) {
                maxValue = result;
                bestSequence = permutation;
            }
        }

        System.out.println("Best sequence: " + bestSequence);
        System.out.println("Maximum value: " + maxValue);
    }

    private static double calculate(int startNum, String[] operations) {
        double result = startNum;
        for (String operation : operations) {
            char op = getOperation(operation);
            double value = getValue(operation);
            switch (op) {
                case '+':
                    if (result > value) {
                        result += value;
                        break;
                    } else {
                        System.out.println("startNum =" + startNum + " value =" + value);
                        result = 0.00;
                        break;
                    }
                case '-':
                    result -= value;
                    break;
                case '*':
                    result *= value;
                    break;
                case '/':
                    result /= value;
                    break;
            }
        }
        return result;
    }

    private static char getOperation(String operation) {
        return operation.charAt(0);
    }

    private static double getValue(String operation) {
        return Double.parseDouble(operation.substring(1));
    }

    private static List<String> generatePermutations(List<String> elements) {
        if (elements.size() == 1) {
            return elements;
        }

        List<String> permutations = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++) {
            String element = elements.get(i);
            List<String> subList = new ArrayList<>(elements);
            subList.remove(i);
            for (String permutation : generatePermutations(subList)) {
                permutations.add(element + "," + permutation);
            }
        }
        return permutations;
    }
}

